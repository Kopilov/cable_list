package com.github.kopilov.cable_list.parser

import krangl.DataFrame
import krangl.readExcel

fun main() {
    val dataFrame = DataFrame.readExcel("parser/src/test/resources/cable-list ALL E20.xlsx")
    println(dataFrame.cols)
}
