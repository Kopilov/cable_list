mkdir -p /var/local/cablelist/postgresql
chmod a+rwx /var/local/cablelist/postgresql

mkdir -p /var/local/cablelist/pgadmin
chmod a+rwx /var/local/cablelist/pgadmin

podman network create --subnet="192.168.1.0/24" localpods
podman play kube --network=localpods --ip=192.168.1.10 pod.yaml

# echo "@reboot podman pod start cablelist" >> /etc/crontab
